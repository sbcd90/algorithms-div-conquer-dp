package com.coursera.week3;

import java.util.*;

class PrefixCode {
    String val;
    int edge;
    PrefixCode left;
    PrefixCode right;
    PrefixCode(String val, int edge) {
        this.val = val;
        this.edge = edge;
    }
    PrefixCode(String val, int edge, PrefixCode left, PrefixCode right) {
        this.val = val;
        this.edge = edge;
        this.left = left;
        this.right = right;
    }
}

public class HuffmanCodes {

    private String charStr;
    private Map<String, Double> charPercent;

    public HuffmanCodes(String charStr) {
        this.charStr = charStr;
        this.charPercent = new HashMap<>();
        int size = charStr.length();

        for (int i = 0; i < size; ++i) {
            if (charPercent.containsKey(String.valueOf(charStr.charAt(i)))) {
                charPercent.put(String.valueOf(charStr.charAt(i)), charPercent.get(String.valueOf(charStr.charAt(i))) + 1);
            } else {
                charPercent.put(String.valueOf(charStr.charAt(i)), 1.0);
            }
        }
        for (var charPercentEntry: charPercent.entrySet()) {
            charPercent.put(charPercentEntry.getKey(), (charPercentEntry.getValue() * 100.0) / size);
        }
    }

    public PrefixCode constructPrefixTree() {
        var pq = new PriorityQueue<>((Comparator<AbstractMap.Entry<PrefixCode, Double>>) (o1, o2) -> (int) (o1.getValue() - o2.getValue()));
        for (Map.Entry<String, Double> charPercentEntry: charPercent.entrySet()) {
            pq.add(new AbstractMap.SimpleEntry<>(new PrefixCode(charPercentEntry.getKey(), 0), charPercentEntry.getValue()));
        }

        while (!pq.isEmpty()) {
            var lowestFreq = pq.poll();
            if (pq.isEmpty()) {
                return lowestFreq.getKey();
            }
            var secondLowestFreq = new AbstractMap.SimpleEntry<>(new PrefixCode(" ", 0), 0.0);
            if (!pq.isEmpty()) {
                var entry = pq.poll();
                secondLowestFreq = new AbstractMap.SimpleEntry<>(new PrefixCode(entry.getKey().val, entry.getKey().edge,
                        entry.getKey().left, entry.getKey().right), entry.getValue());
            }

            var lowestNode = new PrefixCode(lowestFreq.getKey().val, 0, lowestFreq.getKey().left, lowestFreq.getKey().right);
            var secondLowestNode = !secondLowestFreq.getKey().val.equals(" ")? new PrefixCode(secondLowestFreq.getKey().val, 1,
                    secondLowestFreq.getKey().left, secondLowestFreq.getKey().right): null;


            var node = secondLowestNode != null? new AbstractMap.SimpleEntry<>(new PrefixCode(lowestNode.val + secondLowestNode.val, 0, lowestNode, secondLowestNode),
                    lowestFreq.getValue() + secondLowestFreq.getValue()):
                    new AbstractMap.SimpleEntry<>(new PrefixCode(lowestNode.val, 0, lowestNode.left, lowestNode.right), lowestFreq.getValue());
            pq.add(node);
        }

        return null;
    }

    public double calculateValueOfPrefixTree(PrefixCode node) {
        var nodesList = new ArrayList<PrefixCode>();
        nodesList.add(node);

        var height = 0;
        var res = 0.0;
        while (!nodesList.isEmpty()) {
            var newNodes = new ArrayList<PrefixCode>();
            int size = nodesList.size();
            for (int i = 0; i < size; ++i) {
                var currNode = nodesList.get(i);
                if (charPercent.containsKey(currNode.val)) {
                    res += height * (charPercent.get(currNode.val) / 100.0);
                }

                if (currNode.left != null) {
                    newNodes.add(currNode.left);
                }
                if (currNode.right != null) {
                    newNodes.add(currNode.right);
                }
            }

            nodesList.clear();
            if (newNodes.size() > 0) {
                nodesList.addAll(newNodes);
            }
            ++height;
        }
        return res;
    }

    public static void main(String[] args) {
        var charStr = new StringBuilder();
        for (int i = 0; i < 35; ++i) {
            charStr.append('A');
        }
        for (int i = 0; i < 25; ++i) {
            charStr.append('B');
        }
        for (int i = 0; i < 20; ++i) {
            charStr.append('C');
        }
        for (int i = 0; i < 15; ++i) {
            charStr.append('D');
        }
        for (int i = 0; i < 5; ++i) {
            charStr.append('E');
        }
        var huffmanCodes = new HuffmanCodes(charStr.toString());
        var root = huffmanCodes.constructPrefixTree();
        System.out.println(huffmanCodes.calculateValueOfPrefixTree(root));
    }
}