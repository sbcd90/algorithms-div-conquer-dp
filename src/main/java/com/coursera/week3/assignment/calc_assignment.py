from calc_makespan import compute_makespan
import heapq


def greedy_makespan_min(times, m):
    # times is a list of n jobs.
    assert len(times) >= 1
    assert all(elt >= 0 for elt in times)
    assert m >= 2
    n = len(times)
    # please do not reorder the jobs in times or else tests will fail.
    # you can implement a priority queue if you would like.
    # use https://docs.python.org/3/library/heapq.html heapq data structure
    # Return a tuple of two things:
    #    - Assignment list of n numbers from 0 to m-1
    #    - The makespan of your assignment
    # your code here
    processors = [(0, i) for i in range(m)]

    heapq.heapify(processors)
    assignments = []

    for i in range(len(times)):
        processor = heapq.nsmallest(1, processors)[0]
        heapq.heappop(processors)

        assignments.append(processor[1])
        heapq.heappush(processors, (processor[0] + times[i], processor[1]))
        heapq.heapify(processors)

    return assignments, heapq.nlargest(1, processors)[0][0]


def do_test(times, m, expected):
    (a, makespan) = greedy_makespan_min(times, m)
    print('\t Assignment returned: ', a)
    print('\t Claimed makespan: ', makespan)
    assert compute_makespan(times, m, a) == makespan, 'Assignment returned is not consistent with the reported makespan'
    assert makespan == expected, f'Expected makespan should be {expected}, your core returned {makespan}'
    print('Passed')


if __name__ == "__main__":
    print('Test 1:')
    times = [2, 2, 2, 2, 2, 2, 2, 2, 3]
    m = 3
    expected = 7
    do_test(times, m, expected)

    print('Test 2:')
    times = [1] * 20 + [5]
    m = 5
    expected = 9
    do_test(times, m, expected)

    print('Test 3:')
    times = [1] * 40 + [2]
    m = 20
    expected = 4
    do_test(times, m, expected)
    print('All tests passed: 15 points!')
