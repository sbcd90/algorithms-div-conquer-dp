package com.coursera.week2;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LongestCommonSubseq {

    private String s1;
    private String s2;

    public LongestCommonSubseq(String s1, String s2) {
        this.s1 = s1;
        this.s2 = s2;
    }

    public AbstractMap.SimpleEntry<String, List<AbstractMap.SimpleEntry<Integer, Integer>>> memoizeLcs() {
        int m = s1.length();
        int n = s2.length();
        var memoTbl = new int[m+1][n+1];
        var solInfo = new String[m+1][n+1];

        for (int i = m-1; i >= 0; --i) {
            for (int j = n-1; j >= 0; --j) {
                if (s1.charAt(i) == s2.charAt(j)) {
                    memoTbl[i][j] = 1 + memoTbl[i+1][j+1];
                    solInfo[i][j] = "match";
                } else {
                    var opt1 = memoTbl[i+1][j];
                    var sOpt1 = "down";
                    var opt2 = memoTbl[i][j+1];
                    var sOpt2 = "right";

                    if (opt1 > opt2) {
                        memoTbl[i][j] = opt1;
                        solInfo[i][j] = sOpt1;
                    } else {
                        memoTbl[i][j] = opt2;
                        solInfo[i][j] = sOpt2;
                    }
                }
            }
        }

        var lcs = new StringBuilder();
        var matchLocations = new ArrayList<AbstractMap.SimpleEntry<Integer, Integer>>();
        var i = 0;
        var j = 0;
        while (i < m && j < n) {
            if (solInfo[i][j].equals("match")) {
                assert s1.charAt(i) == s2.charAt(j);
                lcs.append(s1.charAt(i));
                matchLocations.add(new AbstractMap.SimpleEntry<>(i, j));
                ++i;
                ++j;
            } else if (solInfo[i][j].equals("down")) {
                ++i;
            } else if (solInfo[i][j].equals("right")) {
                ++j;
            }
        }
        return new AbstractMap.SimpleEntry<>(lcs.toString(), matchLocations);
    }

    public static void main(String[] args) {
        var lcs = new LongestCommonSubseq("GGATTACCATTATGGAGGCGGA", "ACTTAGGTAGG");
        var res = lcs.memoizeLcs();
        System.out.println(res.getKey());
        System.out.println(Arrays.toString(res.getValue().toArray()));
    }
}