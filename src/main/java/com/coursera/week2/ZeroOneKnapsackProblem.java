package com.coursera.week2;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@FunctionalInterface
interface GetTblEntry {
    public Double getTblEntry(int w, int j);
}

public class ZeroOneKnapsackProblem {

    private int maxWeight;
    private int[] weights;
    private double[] values;

    public ZeroOneKnapsackProblem(int maxWeight, int[] weights, double[] values) {
        this.maxWeight = maxWeight;
        this.weights = weights;
        this.values = values;
    }

    public AbstractMap.SimpleEntry<Double, List<Integer>> memoizedMaxStealZeroOne() {
        int size = weights.length;
        assert values.length == size;
        assert maxWeight >= 0;
        if (maxWeight == 0) {
            return new AbstractMap.SimpleEntry<>(0.0, List.of());
        }

        double[][] t = new double[maxWeight+1][size];
        var s = new int[maxWeight+1][size];

        GetTblEntry getTblEntry = (int w, int j) -> {
          if (w == 0) {
              return 0.0;
          }
          if (w < 0) {
              return Double.MIN_VALUE;
          }
          if (j >= size) {
              return 0.0;
          }
          return t[w][j];
        };

        for (int w = 1; w < maxWeight+1; ++w) {
            for (int j = size-1; j >= 0; --j) {
                double opt1 = values[j] + getTblEntry.getTblEntry(w - weights[j], j+1);
                var sOpt1 = 1;
                var opt2 = getTblEntry.getTblEntry(w, j+1);
                var sOpt2 = 0;
                if (opt1 > opt2) {
                    t[w][j] = opt1;
                    s[w][j] = sOpt1;
                } else {
                    t[w][j] = opt2;
                    s[w][j] = sOpt2;
                }
            }
        }

        var itemsToSteal = new ArrayList<Integer>();
        var weightOfKnapsack = maxWeight;
        for (int j = 0; j < size; ++j) {
            if (s[weightOfKnapsack][j] == 1) {
                itemsToSteal.add(j);
                weightOfKnapsack -= weights[j];
            }
        }
        return new AbstractMap.SimpleEntry<>(t[maxWeight][0], itemsToSteal);
    }

    public static void main(String[] args) {
        var values = new double[]{15, 14.5, 19.2, 19.8, 195.2};
        var weights = new int[]{1, 5, 20, 35, 90};
        var knapsackProb = new ZeroOneKnapsackProblem(200, weights, values);
        var res = knapsackProb.memoizedMaxStealZeroOne();
        System.out.println(res.getKey());
        System.out.println(Arrays.toString(res.getValue().toArray(new Integer[0])));
    }
}