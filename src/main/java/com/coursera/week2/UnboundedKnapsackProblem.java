package com.coursera.week2;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UnboundedKnapsackProblem {

    private int maxWeight;
    private int[] weights;
    private double[] values;

    public UnboundedKnapsackProblem(int maxWeight, int[] weights, double[] values) {
        this.maxWeight = maxWeight;
        this.weights = weights;
        this.values = values;
    }

    public AbstractMap.SimpleEntry<Double, List<Integer>> maxStealMemo() {
        var t = new double[maxWeight+1];
        var s = new int[maxWeight+1];
        for (int i = 0; i < maxWeight+1; ++i) {
            s[i] = -1;
        }

        int k = weights.length;
        assert values.length == k;

        for (int w = 0; w < maxWeight+1; ++w) {
            var opts = new ArrayList<AbstractMap.SimpleEntry<Double, Integer>>();
            for (int i = 0; i < k; ++i) {
                if (w - weights[i] >= 0) {
                    opts.add(new AbstractMap.SimpleEntry<>(values[i] + t[w - weights[i]], i));
                }
            }
            opts.add(new AbstractMap.SimpleEntry<>(Double.MIN_VALUE, -1));

            var max = Double.MIN_VALUE;
            var maxS = -1;
            for (int i = 0; i < opts.size(); ++i) {
                if (max < opts.get(i).getKey()) {
                    max = opts.get(i).getKey();
                    maxS = opts.get(i).getValue();
                }
            }
            t[w] = max;
            s[w] = maxS;
        }

        var stolenItemIds = new ArrayList<Integer>();
        var weightRemaining = maxWeight;
        while (weightRemaining >= 0) {
            var itemId = s[weightRemaining];
            if (itemId >= 0) {
                stolenItemIds.add(itemId);
                weightRemaining -= weights[itemId];
            } else {
                break;
            }
        }
        return new AbstractMap.SimpleEntry<>(t[maxWeight], stolenItemIds);
    }

    public static void main(String[] args) {
        var weights = new int[]{1, 5, 20, 35, 90};
        var values = new double[]{15, 14.5, 19.2, 19.8, 195.2};
        var unboundedKnapsack = new UnboundedKnapsackProblem(200, weights, values);
        var res = unboundedKnapsack.maxStealMemo();
        System.out.println(res.getKey());
        System.out.println(Arrays.toString(res.getValue().toArray(new Integer[0])));
    }
}