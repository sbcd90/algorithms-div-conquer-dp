def memoTargetSum(S, tgt):
    k = len(S)
    assert tgt >= 0
    ## Fill in base case for T[(i,j)] where i == k
    T = {}  # Memo table initialized as empty dictionary
    for j in range(tgt + 1):
        T[(k, j)] = j
    # your code here
    for i in range(k-1, -1, -1):
        if i == k-1:
            for j in range(tgt + 1):
                if j == 0 or j == S[i]:
                    T[(i, j)] = 1
                else:
                    T[(i, j)] = 0
        else:
            for j in range(tgt + 1):
                if T[(i+1, j)] == 1:
                    T[(i, j)] = 1
                elif j - S[i] >= 0 and T[(i+1, j - S[i])] == 1:
                    T[(i, j)] = 1
                else:
                    T[(i, j)] = 0

    for j in range(tgt, -1, -1):
        if T[(0, j)] != 0:
            T[(0, tgt)] = tgt - j
            break
    return T


def checkMemoTblTargetSum(a, tgt, expected):
    T = memoTargetSum(a, tgt)
    for i in range(len(a) + 1):
        for j in range(tgt + 1):
            assert (i, j) in T, f'Memo table fails to have entry for i, j = {(i, j)}'
    assert T[(0, tgt)] == expected, f'Expected answer = {expected}, your code returns {T[(0, tgt)]}'
    return


if __name__ == "__main__":
    print('--test 1--')
    a1 = [1, 2, 3, 4, 5, 10]
    print(a1, 15)
    checkMemoTblTargetSum(a1, 15, 0)

    print('--test 2--')
    a2 = [1, 2, 3, 4, 5, 10]
    print(a2, 26)
    checkMemoTblTargetSum(a2, 26, 1)

    print('--test3--')
    a3 = [11, 23, 37, 48, 94, 152, 230, 312, 339, 413]
    print(a3, 457)
    checkMemoTblTargetSum(a3, 457, 1)

    print('--test4--')
    print(a3, 512)
    checkMemoTblTargetSum(a3, 512, 0)

    print('--test5--')
    print(a3, 616)
    checkMemoTblTargetSum(a3, 616, 1)
    print('All tests passed (10 points)!')
