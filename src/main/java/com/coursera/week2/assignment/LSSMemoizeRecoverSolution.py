def computeLSS(a):
    # your code here
    T = {}  # Initialize the memo table to empty dictionary
    # Now populate the entries for the base case
    n = len(a)
    for j in range(-1, n):
        T[(n, j)] = (0, 0)  # i = n and j
    # Now fill out the table : figure out the two nested for loops
    # It is important to also figure out the order in which you iterate the indices i and j
    # Use the recurrence structure itself as a guide: see for instance that T[(i,j)] will depend on T[(i+1, j)]
    # your code here
    for i in range(n - 1, -1, -1):
        for j in range(n - 1, i, -1):
            if j == n - 1:
                if abs(a[i] - a[j]) <= 1:
                    T[(i, j)] = (T[(i + 1, j)][0] + 1, i + 1)
                else:
                    T[(i, j)] = (1, i)
            else:
                if abs(a[i] - a[j]) <= 1:
                    T[(i, j)] = max((T[(j, j)][0] + 1, j), T[(i, j + 1)])
                else:
                    T[(i, j)] = T[(i, j + 1)]
        if i == n - 1:
            T[(i, i)] = (1, i)
        else:
            T[(i, i)] = T[(i, i + 1)]

        for j in range(i - 1, -2, -1):
            T[(i, j)] = T[(i, j + 1)]

    sub = [a[0]]
    i = 0
    while i < n:
        if i == T[(i, -1)][1]:
            break
        sub.append(a[T[(i, -1)][1]])
        i = T[(i, -1)][1]

    return sub


## BEGIN TESTS
def checkSubsequence(a, b):
    i = 0
    j = 0
    n = len(a)
    m = len(b)
    for j in range(m - 1):
        assert abs(b[j] - b[j + 1]) <= 1
    while (i < n and j < m):
        if a[i] == b[j]:
            j = j + 1
        i = i + 1
    if j < m:
        return False
    return True


if __name__ == "__main__":
    print('--Test 1 --')
    a1 = [1, 4, 2, -2, 0, -1, 2, 3]
    print(a1)
    sub1 = computeLSS(a1)
    print(f'sub1 = {sub1}')
    assert len(sub1) == 4, f'Subsequence does not have length 4'
    assert checkSubsequence(a1, sub1), f'Your solution is not a subsequence of the original sequence'

    print('--Test2--')
    a2 = [1, 2, 3, 4, 0, 1, -1, -2, -3, -4, 5, -5, -6]
    print(a2)
    sub2 = computeLSS(a2)
    print(f'sub2 = {sub2}')
    assert len(sub2) == 8
    assert checkSubsequence(a2, sub2)

    print('--Test3--')
    a3 = [0, 2, 4, 6, 8, 10, 12]
    print(a3)
    sub3 = computeLSS(a3)
    print(f'sub3 = {sub3}')
    assert len(sub3) == 1
    assert checkSubsequence(a3, sub3)

    print('--Test4--')
    a4 = [4, 8, 7, 5, 3, 2, 5, 6, 7, 1, 3, -1, 0, -2, -3, 0, 1, 2, 1, 3, 1, 0, -1, 2, 4, 5, 0, 2, -3, -9, -4, -2, -3,
          -1]
    print(a4)
    sub4 = computeLSS(a4)
    print(f'sub4 = {sub4}')
    assert len(sub4) == 14
    assert checkSubsequence(a4, sub4)

    print('All test passed (10 points)')
    ## END TESTS
