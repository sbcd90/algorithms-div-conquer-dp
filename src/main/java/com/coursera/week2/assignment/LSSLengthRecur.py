def lssLength(a, i, j):
    aj = a[j] if 0 <= j < len(a) else None
    # Implement the recurrence below. Use recursive calls back to lssLength
    # your code here
    if i == len(a):
        return 0
    elif i < len(a) and aj is not None and abs(a[i] - aj) > 1:
        return lssLength(a, i + 1, j)
    elif i < len(a) and (aj is None or abs(a[i] - aj) <= 1):
        return max(lssLength(a, i + 1, i) + 1, lssLength(a, i + 1, j))


if __name__ == "__main__":
    print('--Test1--')
    n1 = lssLength([1, 4, 2, -2, 0, -1, 2, 3], 0, -1)
    print(n1)
    assert n1 == 4, f'Test 1 failed: expected answer 4, your code: {n1}'
    print('passed')

    print('--Test2--')
    n2 = lssLength([1, 2, 3, 4, 0, 1, -1, -2, -3, -4, 5, -5, -6], 0, -1)
    print(n2)
    assert n2 == 8, f'Test 2 failed: expected answer 8, your code: {n2}'

    print('--Test3--')
    n3 = lssLength([0, 2, 4, 6, 8, 10, 12], 0, -1)
    print(n3)
    assert n3 == 1, f'Test 3 failed: expected answer 1, your code: {n3}'

    print('--Test 4--')
    n4 = lssLength(
        [4, 8, 7, 5, 3, 2, 5, 6, 7, 1, 3, -1, 0, -2, -3, 0, 1, 2, 1, 3, 1, 0, -1, 2, 4, 5, 0, 2, -3, -9, -4, -2, -3,
         -1], 0, -1)
    print(n4)
    assert n4 == 14, f'Test 4 failed: expected answer 14, your code: {n4}'

    print('All Tests Passed (8 points)')
