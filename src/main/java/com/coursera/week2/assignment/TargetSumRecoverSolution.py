def getBestTargetSum(S, tgt):
    k = len(S)
    assert tgt >= 0
    # your code here
    T = {}
    for i in range(k - 1, -1, -1):
        if i == k - 1:
            for j in range(tgt + 1):
                if j == 0 or j == S[i]:
                    T[(i, j)] = (1, "left")
                else:
                    T[(i, j)] = (0, "down")
        else:
            for j in range(tgt + 1):
                if T[(i + 1, j)][0] == 1:
                    T[(i, j)] = (1, "down")
                elif j - S[i] >= 0 and T[(i + 1, j - S[i])][0] == 1:
                    T[(i, j)] = (1, "left")
                else:
                    T[(i, j)] = (0, "")

    res = []
    for j in range(tgt, -1, -1):
        if T[(0, j)][0] != 0:
            i = 0
            u = j
            while u > 0 and i < k:
                path = T[(i, u)][1]
                if path == "left":
                    res.append(S[i])
                    u = u - S[i]
                i = i + 1
            break
    return res


def checkTgtSumRes(a, tgt, expected):
    a = sorted(a)
    res = getBestTargetSum(a, tgt)
    res = sorted(res)
    print('Your result:', res)
    assert tgt - sum(res) == expected, f'Your code returns result that sums up to {sum(res)}, expected was {expected}'
    i = 0
    j = 0
    n = len(a)
    m = len(res)
    while (i < n and j < m):
        if a[i] == res[j]:
            j = j + 1
        i = i + 1
    assert j == m, 'Your result  {res} is not a subset of {a}'


if __name__ == "__main__":
    print('--test 1--')
    a1 = [1, 2, 3, 4, 5, 10]
    print(a1, 15)
    checkTgtSumRes(a1, 15, 0)

    print('--test 2--')
    a2 = [1, 8, 3, 4, 5, 12]
    print(a2, 26)
    checkTgtSumRes(a2, 26, 0)

    print('--test 3--')
    a3 = [8, 3, 2, 4, 5, 7, 12]
    print(a3, 38)
    checkTgtSumRes(a3, 38, 0)

    print('--test 4 --')
    a4 = sorted([1, 10, 19, 18, 12, 11, 0, 9, 16, 17, 2, 7, 14, 29, 38, 45, 13, 26, 51, 82, 111, 124, 135, 189])
    print(a4)
    checkTgtSumRes(a4, 155, 0)
    print('--test 5--')
    checkTgtSumRes(a4, 189, 0)

    print('--test 7--')
    checkTgtSumRes(a4, 347, 0)

    print('--test 8--')
    checkTgtSumRes(a4, 461, 0)

    print('--test 9--')
    checkTgtSumRes(a4, 462, 0)

    print('--test 9--')
    checkTgtSumRes(a4, 517, 0)

    print('--test 10--')
    checkTgtSumRes(a4, 975, 3)

    print('All Tests Passed (15 points)')
