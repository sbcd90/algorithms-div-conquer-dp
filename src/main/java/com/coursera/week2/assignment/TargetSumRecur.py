def targetSum(S, i, tgt):
    # your code here
    s_solution = []
    global res
    res = 0
    targetSumRecur(S, i, tgt, s_solution)
    return tgt - res


def targetSumRecur(S, i, tgt, s_solution):
    n = len(S)

    if i == n:
        opt = 0
        for j in range(n):
            if s_solution[j] == 1:
                opt = opt + S[j]

        global res
        if res < opt:
            res = opt
        return
    else:
        sum_so_far = 0
        for j in range(0, i):
            if s_solution[j] == 1:
                sum_so_far = sum_so_far + S[j]

        for j in (0, 1):
            if j == 0 or (j == 1 and sum_so_far + S[i] <= tgt):
                s_solution.append(j)
                targetSumRecur(S, i + 1, tgt, s_solution)
                s_solution.pop()


def tgtSum(tgt, S):
    return targetSum(S, 0, tgt)


if __name__ == "__main__":
    t1 = tgtSum(15, [1, 2, 3, 4, 5, 10])  # Should be zero
    assert t1 == 0, 'Test 1 failed'

    t2 = tgtSum(26, [1, 2, 3, 4, 5, 10])  # should be 1
    assert t2 == 1, 'Test 2 failed'

    t3 = (tgtSum(23, [1, 2, 3, 4, 5, 10]))  # should be 0
    assert t3 == 0, 'Test 3 failed'

    t4 = (tgtSum(18, [1, 2, 3, 4, 5, 10]))  # should be 0
    assert t4 == 0, 'Test 4 failed'

    t5 = (tgtSum(9, [1, 2, 3, 4, 5, 10]))  # should be 0
    assert t5 == 0, 'Test 5 failed'

    t6 = (tgtSum(457, [11, 23, 37, 48, 94, 152, 230, 312, 339, 413]))  # should be 1
    assert t6 == 1, 'Test 6 failed'

    t7 = (tgtSum(512, [11, 23, 37, 48, 94, 152, 230, 312, 339, 413]))  # should be 0
    assert t7 == 0, 'Test 7 failed'

    t8 = (tgtSum(616, [11, 23, 37, 48, 94, 152, 230, 312, 339, 413]))  # should be 1
    assert t8 == 1, 'Test 8 failed'

    print('All tests passed (10 points)!')
