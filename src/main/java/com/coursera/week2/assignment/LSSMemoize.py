def memoizeLSS(a):
    T = {}  # Initialize the memo table to empty dictionary
    # Now populate the entries for the base case
    n = len(a)
    for j in range(-1, n):
        T[(n, j)] = 0  # i = n and j
    # Now fill out the table : figure out the two nested for loops
    # It is important to also figure out the order in which you iterate the indices i and j
    # Use the recurrence structure itself as a guide: see for instance that T[(i,j)] will depend on T[(i+1, j)]
    # your code here
    for i in range(n-1, -1, -1):
        for j in range(n-1, i, -1):
            if j == n-1:
                if abs(a[i] - a[j]) <= 1:
                    T[(i, j)] = T[(i+1, j)] + 1
                else:
                    T[(i, j)] = 1
            else:
                if abs(a[i] - a[j]) <= 1:
                    T[(i, j)] = max(T[(j, j)] + 1, T[(i, j+1)])
                else:
                    T[(i, j)] = T[(i, j+1)]
        if i == n-1:
            T[(i, i)] = 1
        else:
            T[(i, i)] = T[(i, i+1)]

        for j in range(i-1, -2, -1):
            T[(i, j)] = T[(i, j+1)]

    return T


def lssLength(a, i, j):
    assert False, 'Redefining lssLength: You should not be calling this function from your memoization code'


def checkMemoTableHasEntries(a, T):
    for i in range(len(a) + 1):
        for j in range(i):
            assert (i, j) in T, f'entry for {(i, j)} not in memo table'


def checkMemoTableBaseCase(a, T):
    n = len(a)
    for j in range(-1, n):
        assert T[(n, j)] == 0, f'entry for {(n, j)} is not zero as expected'


if __name__ == "__main__":
    print('-- Test 1 -- ')
    a1 = [1, 4, 2, -2, 0, -1, 2, 3]
    print(a1)
    T1 = memoizeLSS(a1)
    checkMemoTableHasEntries(a1, T1)
    checkMemoTableBaseCase(a1, T1)
    assert T1[(0, -1)] == 4, f'Test 1: Expected answer is 4. your code returns {T1[(0, -1)]}'
    print('Passed')

    print('--Test2--')
    a2 = [1, 2, 3, 4, 0, 1, -1, -2, -3, -4, 5, -5, -6]
    print(a2)
    T2 = memoizeLSS(a2)
    checkMemoTableHasEntries(a2, T2)
    checkMemoTableBaseCase(a2, T2)
    assert T2[(0, -1)] == 8, f'Test 2: Expected answer is 8. Your code returns {T2[(0, -1)]}'

    print('--Test3--')
    a3 = [0, 2, 4, 6, 8, 10, 12]
    print(a3)
    T3 = memoizeLSS(a3)
    checkMemoTableHasEntries(a3, T3)
    checkMemoTableBaseCase(a3, T3)
    assert T3[(0, -1)] == 1, f'Test 3: Expected answer is  1. Your code returns {T3[(0, -1)]}'

    print('--Test4--')
    a4 = [4, 8, 7, 5, 3, 2, 5, 6, 7, 1, 3, -1, 0, -2, -3, 0, 1, 2, 1, 3, 1, 0, -1, 2, 4, 5, 0, 2, -3, -9, -4, -2, -3,
          -1]
    print(a4)
    T4 = memoizeLSS(a4)
    checkMemoTableHasEntries(a4, T4)
    checkMemoTableBaseCase(a4, T4)
    assert T4[(0, -1)] == 14, f'Text 4: Expected answer is 14. Your code returns {T4[(0, -1)]}'

    print('All tests passed (7 points)')
