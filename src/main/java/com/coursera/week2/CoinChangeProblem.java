package com.coursera.week2;

import org.apache.commons.lang3.ArrayUtils;

import java.util.*;

public class CoinChangeProblem {

    private int[] coins;
    private int amount;

    public CoinChangeProblem(int[] coins, int amount) {
        this.coins = coins;
        this.amount = amount;
    }

    public AbstractMap.SimpleEntry<Integer, List<Integer>> findMinCoins() {
        int size = coins.length;
        var memoTbl = new int[amount+1][size+1];
        var solutionTbl = new int[amount+1][size+1];

        for (int i = 0; i < amount+1; ++i) {
            for (int j = 0; j < size+1; ++j) {
                solutionTbl[i][j] = -1;
            }
        }

        for (int y=1; y < amount+1; ++y) {
            memoTbl[y][0] = 1000000;
            for (int j = 1; j < size+1; ++j) {
                var coin = coins[j-1];

                if (y < coin) {
                    memoTbl[y][j] = memoTbl[y][j-1];
                    solutionTbl[y][j] = 0;
                } else {
                    var p = y / coin;
                    assert p > 0;
                    var minValue = 1000000;
                    var bestOption = -1;

                    for (int i = 0; i < p+1; ++i) {
                        var l = i + memoTbl[y-i*coin][j-1];
                        if (l < minValue) {
                            minValue = l;
                            bestOption = i;
                        }
                    }
                    memoTbl[y][j] = minValue;
                    solutionTbl[y][j] = bestOption;
                }
            }
        }

        var res = new AbstractMap.SimpleEntry<Integer, List<Integer>>(memoTbl[amount][size], null);

        var j = size;
        var y = amount;
        var optimalPath = new ArrayList<Integer>();
        while (y > 0 && j >= 0) {
            var k = solutionTbl[y][j];
            optimalPath.add(k);
            y -= k * coins[j-1];
            --j;
        }
        Collections.reverse(optimalPath);
        res.setValue(optimalPath);
        return res;
    }

    public static void main(String[] args) {
        var coins = new int[]{1, 4, 7, 9, 16, 43};
        var coinChangeProb = new CoinChangeProblem(coins, 33);
        var res = coinChangeProb.findMinCoins();
        System.out.println(res.getKey());
        System.out.println(Arrays.toString(res.getValue().toArray(new Integer[0])));
    }
}