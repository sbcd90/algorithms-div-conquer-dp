package com.coursera.week2;

import org.apache.commons.lang3.ArrayUtils;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NaiveRodCutting {

    private int l;
    private double[] prices;
    private int[] sizes;

    public NaiveRodCutting(int l, double[] prices, int[] sizes) {
        this.l = l;
        this.prices = prices;
        this.sizes = sizes;
    }

    public double maxRevenueRecursive() {
        return maxRevenueRecursive(l);
    }

    public double maxRevenueRecursive(int l) {
        if (l == 0) {
            return 0.0;
        } else if (l < 0) {
            return Integer.MIN_VALUE;
        }

        var k = sizes.length;
        assert prices.length == k;

        var maxOpt = 0.0;
        for (int i = 0; i < k; ++i) {
            var opt = prices[i] + maxRevenueRecursive(l - sizes[i]);
            maxOpt = Math.max(maxOpt, opt);
        }
        return maxOpt;
    }

    public AbstractMap.SimpleEntry<Double, List<Integer>> maxRevenueMemoizeWithSolutionRecovery() {
        var t = new double[l+1];
        var s = new int[l+1];
        for (int i = 0; i < l+1; ++i) {
            s[i] = -1;
        }

        int k = sizes.length;
        assert prices.length == k;

        for (int lIdx = 1; lIdx < l+1; ++lIdx) {
            t[lIdx] = 0;

            var optionsWithSolutions = new ArrayList<AbstractMap.SimpleEntry<Double, Integer>>();
            for (int i = 0; i < k; ++i) {
                if (lIdx - sizes[i] >= 0) {
                    optionsWithSolutions.add(new AbstractMap.SimpleEntry<>(prices[i] + t[lIdx-sizes[i]], i));
                }
            }
            optionsWithSolutions.add(new AbstractMap.SimpleEntry<>(0.0, -1));

            for (int i = 0; i < optionsWithSolutions.size(); ++i) {
                if (t[lIdx] < optionsWithSolutions.get(i).getKey()) {
                    t[lIdx] = Math.max(t[lIdx], optionsWithSolutions.get(i).getKey());
                    s[lIdx] = optionsWithSolutions.get(i).getValue();
                }
            }
        }

        var cuts = new ArrayList<Integer>();
        var lIdx = l;
        while (lIdx > 0) {
            var optionId = s[lIdx];
            if (optionId >= 0) {
                cuts.add(sizes[optionId]);
                lIdx -= sizes[optionId];
            } else {
                break;
            }
        }
        return new AbstractMap.SimpleEntry<>(t[l], cuts);
    }

    public static void main(String[] args) {
        var sizes = new int[]{1, 3, 5, 10, 30, 50, 75};
        var prices = new double[]{0.1, 0.2, 0.4, 0.9, 3.1, 5.1, 8.2};
        var rodCutting = new NaiveRodCutting(30, prices, sizes);
        System.out.println(rodCutting.maxRevenueRecursive());

        var res = rodCutting.maxRevenueMemoizeWithSolutionRecovery();
        System.out.println(res.getKey() + "-" + Arrays.toString(res.getValue().toArray(new Integer[0])));
    }
}