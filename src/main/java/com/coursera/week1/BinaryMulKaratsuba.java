package com.coursera.week1;

import java.util.ArrayList;
import java.util.Arrays;

public class BinaryMulKaratsuba {

    private int[] a;
    private int[] b;

    public BinaryMulKaratsuba(int[] a, int[] b) {
        this.a = a;
        this.b = b;
    }

    public int[] multiply() {
        return multiply(a, b);
    }

    private int[] multiply(int[] num1, int[] num2) {
        int m = num1.length;
        int n = num2.length;

        var tmp = new ArrayList<Integer>();
        var flag = false;
        for (int i = 0; i < m; ++i) {
            if (!flag && num1[i] != 0) {
                flag = true;
            }
            if (flag) {
                tmp.add(num1[i]);
            }
        }
        num1 = new int[tmp.size()];
        for (int i = 0; i < tmp.size(); ++i) {
            num1[i] = tmp.get(i);
        }

        tmp = new ArrayList<>();
        flag = false;
        for (int i = 0; i < n; ++i) {
            if (!flag && num2[i] != 0) {
                flag = true;
            }
            if (flag) {
                tmp.add(num2[i]);
            }
        }
        num2 = new int[tmp.size()];
        for (int i = 0; i < tmp.size(); ++i) {
            num2[i] = tmp.get(i);
        }

        m = num1.length;
        n = num2.length;

        if (m == 0 || n == 0) {
            return new int[]{0};
        } else if (m <= 2 || n <= 2) {
            var mulGradeSchool = new BinaryMulGradeSchool(num1, num2);
            return mulGradeSchool.multiply();
        } else {
            var mid1 = m / 2;
            var a2 = new int[m - mid1];
            System.arraycopy(num1, 0, a2, 0, m - mid1);
            var a1 = new int[mid1];
            System.arraycopy(num1, m - mid1, a1, 0, mid1);

            var b2 = new int[]{};
            var b1 = new int[]{};
            if (n - mid1 <= 0) {
                b2 = new int[]{0};
                b1 = num2;
            } else {
                b2 = new int[n - mid1];
                System.arraycopy(num2, 0, b2, 0, n - mid1);
                b1 = new int[mid1];
                System.arraycopy(num2, n - mid1, b1, 0, mid1);
            }

            var r1 = multiply(Arrays.copyOf(a1, a1.length), Arrays.copyOf(b1, b1.length));
            var r2 = multiply(Arrays.copyOf(a2, a2.length), Arrays.copyOf(b2, b2.length));

            var binAddSum1 = new BinaryNumSum(Arrays.copyOf(a1, a1.length), Arrays.copyOf(a2, a2.length)).add();
            var binAddSum2 = new BinaryNumSum(Arrays.copyOf(b1, b1.length), Arrays.copyOf(b2, b2.length)).add();
            var r4 = multiply(binAddSum1, binAddSum2);

            var r5a = new BinaryNumSubtract(Arrays.copyOf(r4, r4.length), Arrays.copyOf(r1, r1.length)).subtract();
            var r5 = new BinaryNumSubtract(Arrays.copyOf(r5a, r5a.length), Arrays.copyOf(r2, r2.length)).subtract();

            var s1 = pad(Arrays.copyOf(r2, r2.length), 2 * mid1);
            var s2 = pad(Arrays.copyOf(r5, r5.length), mid1);
            var s4 = new BinaryNumSum(Arrays.copyOf(s1, s1.length), Arrays.copyOf(s2, s2.length)).add();
            return new BinaryNumSum(Arrays.copyOf(s4, s4.length), Arrays.copyOf(r1, r1.length)).add();
        }
    }

    private int[] pad(int[] num, int padValue) {
        var res = new int[padValue + num.length];
        System.arraycopy(num, 0, res, 0, num.length);
        return res;
    }

    public static void main(String[] args) {
        var binMulKaratsuba = new BinaryMulKaratsuba(BinaryNumSum.convertDecToBinary(476095), BinaryNumSum.convertDecToBinary(1237));
        var res = binMulKaratsuba.multiply();
        System.out.println(BinaryNumSum.convertBinaryToDec(res));
    }
}