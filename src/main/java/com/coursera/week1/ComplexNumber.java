package com.coursera.week1;

import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.complex.ComplexUtils;

import java.util.Arrays;

public class ComplexNumber extends Complex {

    private double real;
    private double imaginary;

    public ComplexNumber(double real, double imaginary) {
        super(real, imaginary);
    }

    public ComplexNumber(Complex complex) {
        super(complex.getReal(), complex.getImaginary());
    }

    public double[] polar() {
        var r = Math.sqrt(this.getReal() * this.getReal() + this.getImaginary() * this.getImaginary());
        var theta = this.getReal() > 0? Math.atan(this.getImaginary() / this.getReal()):
                Math.atan(this.getImaginary() / this.getReal()) + Math.PI;

        return new double[]{r, theta};
    }

    public static ComplexNumber[] getRootsOfUnity(double n) {
        assert n >= 2;
        var angles = new double[(int) n];

        for (int i = 0; i < (int) n; ++i) {
            angles[i] = 2.0 * Math.PI * (((double) i) / n);
        }

        var lst = new ComplexNumber[(int) n];
        var i = 0;
        for (var angle: angles) {
            var real = Math.cos(angle);
            var imaginary = Math.sin(angle);
            lst[i] = new ComplexNumber(real, imaginary);
            ++i;
        }
        return lst;
    }

    public static void main(String[] args) {
        var z1 = new ComplexNumber(1, 2);
        var z2 = new ComplexNumber(-1, 1);
        var polar2 = z2.polar();
        System.out.println("r- " + polar2[0] + ", theta- " + polar2[1]);

        var z4 = z2.reciprocal();
        System.out.println(z4.getReal() + (z4.getImaginary() < 0.0? "": "+") + z4.getImaginary() + "j");

        var polar4 = new ComplexNumber(z4).polar();
        System.out.println("r- " + polar4[0] + ", theta- " + polar4[1]
                +", complex-" + ComplexUtils.polar2Complex(polar4[0], polar4[1]));

        var z5 = new ComplexNumber(1.0, -2.1);
        System.out.println(z5);

        var rootsOfUnity2 = ComplexNumber.getRootsOfUnity(2);
        System.out.println(Arrays.deepToString(rootsOfUnity2));
        var rootsOfUnity4 = ComplexNumber.getRootsOfUnity(3);
        System.out.println(Arrays.deepToString(rootsOfUnity4));
        var rootsOfUnity6 = ComplexNumber.getRootsOfUnity(6);
        System.out.println(Arrays.deepToString(rootsOfUnity6));
        var rootsOfUnity12 = ComplexNumber.getRootsOfUnity(12);
        System.out.println(Arrays.deepToString(rootsOfUnity12));
    }
}