import sys
from random import randint


def maxSubArray(a):
    n = len(a)
    if n == 1:
        return 0
    # your code here

    min_so_far = sys.maxsize
    res = 0
    for idx in range(n):
        if a[idx] < min_so_far:
            min_so_far = a[idx]

        if res < a[idx] - min_so_far:
            res = a[idx] - min_so_far
    return res


def get_random_array(n):
    assert (n > 100)
    lst = [randint(0, 25) for j in range(n)]
    lst[0] = 1000
    lst[10] = -15
    lst[25] = 40
    lst[n - 10] = 60
    lst[n - 3] = -40
    return lst


if __name__ == "__main__":
    assert (maxSubArray([100, -2, 5, 10, 11, -4, 15, 9, 18, -2, 21, -11]) == 25), 'Test 1 failed'
    assert (maxSubArray([-5, 1, 10, 4, 11, 4, 15, 9, 18, 0, 21, -11]) == 26), 'Test 2 failed'
    assert (maxSubArray([26, 0, 5, 18, 11, -1, 15, 9, 13, 5, 16, -11]) == 18), 'Test 3 failed'
    assert (maxSubArray(get_random_array(50000)) == 75), 'Test on large random array 50000 failed'
    assert (maxSubArray(get_random_array(500000)) == 75), 'Test on large random array of size 500000 failed'
    print('All tests passed (10 points!)')
