import numpy as np
from numpy.fft import fft, ifft
from numpy import real, imag


def polynomial_multiply(a_coeff_list, b_coeff_list):
    # Return the coefficient list of the multiplication
    # of the two polynomials
    # Returned list must be a list of floating point numbers.
    # Please convert list from complex to reals by using the
    # real function in numpy.
    # your code here

    deg_a = len(a_coeff_list) - 1
    deg_b = len(b_coeff_list) - 1

    deg_a_b = 2 * (deg_a + deg_b)
    deg_c = 1 << (deg_a_b - 1).bit_length()

    a_coeff_list_padded = np.pad(a_coeff_list, (0, deg_c - len(a_coeff_list)))
    b_coeff_list_padded = np.pad(b_coeff_list, (0, deg_c - len(b_coeff_list)))

    fft_a = fft(a_coeff_list_padded)
    fft_b = fft(b_coeff_list_padded)
    fft_c = fft_a * fft_b

    c = ifft(fft_c)
    real_c = np.round(np.real(c)).astype(np.int32)
    return np.trim_zeros(real_c, trim='b')


def check_poly(lst1, lst2):
    print(f'Your code found: {lst1}')
    print(f'Expected: {lst2}')
    assert (len(lst1) == len(lst2)), 'Lists have different lengths'
    for (k, j) in zip(lst1, lst2):
        assert abs(k - j) <= 1E-05, 'Polynomials do not match'
    print('Passed!')


if __name__ == "__main__":
    print('-------')
    print('Test # 1')
    # multiply (1 + x - x^3) with (2 - x + x^2)
    a = [1, 1, 0, -1]
    b = [2, -1, 1]
    c = polynomial_multiply(a, b)
    assert (len(c) == 6)
    print(f'c={c}')
    check_poly(c, [2, 1, 0, -1, 1, -1])
    print('-------')
    print('Test # 2')
    # multiply 1 - x + x^2 + 2 x^3 + 3 x^5 with
    #            -x^2 + x^4 + x^6
    a = [1, -1, 1, 2, 0, 3]
    b = [0, 0, -1, 0, 1, 0, 1]
    c = polynomial_multiply(a, b)
    assert (len(c) == 12)
    print(f'c={c}')
    check_poly(c, [0, 0, -1, 1, 0, -3, 2, -2, 1, 5, 0, 3])
    print('-------')
    print('Test # 3')
    # multiply 1 - 2x^3 + x^7 - 11 x^11
    # with     2 - x^4 - x^6 + x^8
    a = [1, 0, 0, -2, 0, 0, 0, 1, 0, 0, 0, -11]
    b = [2, 0, 0, 0, -1, 0, -1, 0, 1]
    c = polynomial_multiply(a, b)
    assert (len(c) == 20)
    print(f'c={c}')
    check_poly(c, [2, 0, 0, -4, -1, 0, -1, 4, 1, 2, 0, -25, 0, -1, 0, 12, 0, 11, 0, -11])
    print('All tests passed (10 points!)')
