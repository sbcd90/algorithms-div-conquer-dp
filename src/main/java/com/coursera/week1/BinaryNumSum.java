package com.coursera.week1;

import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;

public class BinaryNumSum {
    private int[] num1;
    private int[] num2;

    public BinaryNumSum(int[] num1, int[] num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    public int[] add() {
        int minSize = Math.min(num1.length, num2.length);
        int maxSize = Math.max(num1.length, num2.length);

        ArrayUtils.reverse(num1);
        ArrayUtils.reverse(num2);
        var carry = 0;

        var res = new int[maxSize+1];
        for (int i = 0; i < minSize; ++i) {
            var opt = add(num1[i], num2[i], carry);
            res[i] = opt[0];
            carry = opt[1];
        }

        if (minSize < maxSize && num1.length == maxSize) {
            for (int i = minSize; i < maxSize; ++i) {
                var opt = add(num1[i], 0, carry);
                res[i] = opt[0];
                carry = opt[1];
            }
        } else if (minSize < maxSize && num2.length == maxSize) {
            for (int i = minSize; i < maxSize; ++i) {
                var opt = add(0, num2[i], carry);
                res[i] = opt[0];
                carry = opt[1];
            }
        }

        if (carry != 0) {
            res[maxSize] = carry;
        }
        ArrayUtils.reverse(res);
        return res;
    }

    private int[] add(int x, int y, int carry) {
        if (x == 0 && y == 0 && carry == 0) {
            return new int[]{0, 0};
        } else if (x == 0 && y == 0 && carry == 1) {
            return new int[]{1, 0};
        } else if (x == 0 && y == 1 && carry == 0) {
            return new int[]{1, 0};
        } else if (x == 0 && y == 1 && carry == 1) {
            return new int[]{0, 1};
        } else if (x == 1 && y == 0 && carry == 0) {
            return new int[]{1, 0};
        } else if (x == 1 && y == 0 && carry == 1) {
            return new int[]{0, 1};
        } else if (x == 1 && y == 1 && carry == 0) {
            return new int[]{0, 1};
        } else if (x == 1 && y == 1 && carry == 1) {
            return new int[]{1, 1};
        }
        return new int[]{0, 0};
    }

    protected static int convertBinaryToDec(int[] num) {
        int size = num.length;
        var res = 0;
        var pow = 0;
        for (int i = size-1; i >= 0; --i) {
            res += num[i] * Math.pow(2, pow);
            ++pow;
        }
        return res;
    }

    protected static int[] convertDecToBinary(int num) {
        var res = new ArrayList<Integer>();
        while (num != 0) {
            res.add(num % 2);
            num /= 2;
        }

        var resArr = new int[res.size()];
        for (int i = 0; i < res.size(); ++i) {
            resArr[i] = res.get(i);
        }
        ArrayUtils.reverse(resArr);
        return resArr;
    }

    public static void main(String[] args) {
        var binNumAdd = new BinaryNumSum(convertDecToBinary(119304695), convertDecToBinary(1067318907));
        var res = binNumAdd.add();
        System.out.println(convertBinaryToDec(res));
    }
}