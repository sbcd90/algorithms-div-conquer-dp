package com.coursera.week1;

import java.util.Arrays;

public class MaximumSubArrProblem {

    private int[] arr;

    public MaximumSubArrProblem(int[] arr) {
        this.arr = arr;
    }

    public int findMaximumSubArr() {
        return findMaximumSubArr(arr, 0, arr.length);
    }

    public int findMaximumSubArr(int[] arr, int left, int right) {
        var dst = new int[right - left];
        System.arraycopy(arr, left, dst, 0, right - left);

        if (left + 1 == right) {
            System.out.println(Arrays.toString(dst) + " " + arr[left]);
            return arr[left];
        } else if (left + 2 == right) {
            var max = Math.max(0, arr[right-1] - arr[left]);
            System.out.println(Arrays.toString(dst) + " " + max);
            return max;
        } else {
            var mid = left + (right - left) / 2;
            var opt1 = findMaximumSubArr(arr, left, mid);
            var opt2 = findMaximumSubArr(arr, mid, right);

            var opt4 = Integer.MAX_VALUE;
            for (int i = left; i < mid; ++i) {
                if (opt4 > arr[i]) {
                    opt4 = arr[i];
                }
            }

            var opt5 = Integer.MIN_VALUE;
            for (int i = mid; i < right; ++i) {
                if (opt5 < arr[i]) {
                    opt5 = arr[i];
                }
            }

            var max = Math.max(opt5 - opt4, Math.max(opt1, opt2));
            System.out.println(Arrays.toString(dst) + " " + max);
            return max;
        }
    }

    public static void main(String[] args) {
        var prob = new MaximumSubArrProblem(new int[]{1, 19, 5, -4, 7,  18, 15, -10});
        System.out.println(prob.findMaximumSubArr());
    }
}