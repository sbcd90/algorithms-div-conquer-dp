package com.coursera.week1;

import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;

public class BinaryMulGradeSchool {
    private int[] num1;
    private int[] num2;

    public BinaryMulGradeSchool(int[] num1, int[] num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    public int[] multiply() {
        var multiplicand = num1.length >= num2.length ? num1: num2;
        var multiplier = num1.length < num2.length ? num1: num2;

        int size = Math.min(num1.length, num2.length);
        ArrayUtils.reverse(multiplicand);
        ArrayUtils.reverse(multiplier);
        var zeros = new int[multiplicand.length];

        var sumArrayCollect = new ArrayList<int[]>();
        for (int i = 0; i < size; ++i) {
            if (multiplier[i] == 1) {
                sumArrayCollect.add(leftShift(multiplicand, i));
            } else {
                sumArrayCollect.add(leftShift(zeros, i));
            }
        }

        var res = sumArrayCollect.get(0);
        ArrayUtils.reverse(res);
        for (int i = 1; i < size; ++i) {
            ArrayUtils.reverse(sumArrayCollect.get(i));
            var binNumSum = new BinaryNumSum(res, sumArrayCollect.get(i));
            res = binNumSum.add();
        }
        return res;
    }

    private int[] leftShift(int[] num, int cntOfShift) {
        var res = new int[num.length + cntOfShift];
        System.arraycopy(num, 0, res, cntOfShift, num.length);
        return res;
    }

    public static void main(String[] args) {
        var binNumMultiply = new BinaryMulGradeSchool(BinaryNumSum.convertDecToBinary(2367),  BinaryNumSum.convertDecToBinary(567));
        var res = binNumMultiply.multiply();
        System.out.println(BinaryNumSum.convertBinaryToDec(res));
    }
}