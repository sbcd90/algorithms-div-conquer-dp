package com.coursera.week1;

import org.apache.commons.math3.complex.Complex;

import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DiscreteFourierTransform {

    private double[] seq;

    public DiscreteFourierTransform(double[] seq) {
        this.seq = seq;
    }

    public ComplexNumber[] computeFourierTransformCoefficientsNaive() {
        int size = seq.length;
        var fftSeq = new ComplexNumber[size];

        var w = new ComplexNumber(1.0d, 0.0d);
        var wsize = new ComplexNumber(Math.cos(2 * Math.PI / size), Math.sin(2 * Math.PI / size));

        for (int j = 0; j < size; ++j) {
            var Aj = new ComplexNumber(0.0d, 0.0d);
            var wj = new ComplexNumber(1.0d, 0.0d);

            for (int k = 0; k < size; ++k) {
                Aj = new ComplexNumber(Aj.add(wj.multiply(seq[k])));
                wj = new ComplexNumber(wj.multiply(w));
            }
            fftSeq[j] = Aj;
            w = new ComplexNumber(w.multiply(wsize));
        }

        return fftSeq;
    }

    public ComplexNumber[] computeFourierTransformCoefficients() {
        return computeFourierTransformCoefficients(seq);
    }

    public ComplexNumber[] computeFourierTransformCoefficients(double[] sequence) {
        var size = sequence.length;
        if (size == 1) {
            return new ComplexNumber[]{new ComplexNumber(sequence[0], 0.0)};
        } else {
            assert size % 2 == 0;

            var seqEven = new double[size/2];
            var seqOdd = new double[size/2];

            for (int i = 0; i < size/2; ++i) {
                seqEven[i] = sequence[2 * i];
            }
            for (int i = 0; i < size/2; ++i) {
                seqOdd[i] = sequence[2 * i + 1];
            }

            var s1 = computeFourierTransformCoefficients(seqEven);
            var s2 = computeFourierTransformCoefficients(seqOdd);

            var fftRet = new ComplexNumber[size];
            for (int i = 0; i < size; ++i) {
                fftRet[i] = new ComplexNumber(0.0, 0.0);
            }

            var w = new ComplexNumber(1.0, 0.0);
            var wsize = new ComplexNumber(Math.cos(2 * Math.PI / size), Math.sin(2 * Math.PI / size));

            for (int k = 0; k < size / 2; ++k) {
                fftRet[k] = new ComplexNumber(s1[k].add(w.multiply(s2[k])));
                fftRet[k + size/2] = new ComplexNumber(s1[k].subtract(w.multiply(s2[k])));
                w = new ComplexNumber(w.multiply(wsize));
            }
            return fftRet;
        }
    }

    public ComplexNumber[] computeInverseFourierTransform(ComplexNumber[] coefficients) {
        var size = coefficients.length;
        if (size == 1) {
            return coefficients;
        } else {
            assert size % 2 == 0;

            var seqEven = new ComplexNumber[size/2];
            var seqOdd = new ComplexNumber[size/2];

            for (int i = 0; i < size/2; ++i) {
                seqEven[i] = coefficients[2 * i];
            }
            for (int i = 0; i < size/2; ++i) {
                seqOdd[i] = coefficients[2 * i + 1];
            }

            var s1 = computeInverseFourierTransform(seqEven);
            var s2 = computeInverseFourierTransform(seqOdd);

            var fftRet = new ComplexNumber[size];
            for (int i = 0; i < size; ++i) {
                fftRet[i] = new ComplexNumber(0.0, 0.0);
            }

            var w = new ComplexNumber(1.0, 0.0);
            var wsize = new ComplexNumber(Math.cos(2 * Math.PI / size), Math.sin(2 * Math.PI / size));

            for (int k = 0; k < size / 2; ++k) {
                fftRet[k] = new ComplexNumber(s1[k].add(w.multiply(s2[k])).multiply(0.5));
                fftRet[k + size/2] = new ComplexNumber(s1[k].subtract(w.multiply(s2[k])).multiply(0.5));
                w = new ComplexNumber(w.multiply(wsize));
            }
            return fftRet;
        }
    }

    public static void main(String[] args) {
        var seq = new double[]{1.0, -1.0, 1.0, 1.0, 1.0, -1.0,1.0, 1.0};
        var dft = new DiscreteFourierTransform(seq);

        var res = dft.computeFourierTransformCoefficientsNaive();
        System.out.println(Arrays.deepToString(res));

        var inverseRes = Arrays.stream(dft.computeInverseFourierTransform(res)).map(Complex::getReal).collect(Collectors.toList()).toArray(new Double[]{});
        System.out.println(Arrays.deepToString(inverseRes));

        res = dft.computeFourierTransformCoefficients();
        System.out.println(Arrays.deepToString(res));

        var inverseResDivAndConquer = Arrays.stream(dft.computeInverseFourierTransform(res)).map(Complex::getReal).collect(Collectors.toList()).toArray(new Double[]{});
        System.out.println(Arrays.deepToString(inverseResDivAndConquer));
    }
}