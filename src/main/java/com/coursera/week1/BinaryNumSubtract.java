package com.coursera.week1;

import org.apache.commons.lang3.ArrayUtils;

public class BinaryNumSubtract {

    private int[] num1;
    private int[] num2;

    public BinaryNumSubtract(int[] num1, int[] num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    public int[] subtract() {
        int size1 = num1.length;
        int size2 = num2.length;
        int k = size1 - size2;

        var num2Complement = new int[num2.length];
        for (int i = 0; i < num2.length; ++i) {
            num2Complement[i] = 1 - num2[i];
        }

        ArrayUtils.reverse(num2Complement);
        var shiftComplement = leftShift(num2Complement, k);
        ArrayUtils.reverse(shiftComplement);

        var num2Complement2 = new BinaryNumSum(shiftComplement, new int[]{1}).add();
        var finalRes = new BinaryNumSum(num1, num2Complement2).add();

        var res = new int[size1];
        for (int i = finalRes.length - 1, idx = 0; idx < size1; --i, ++idx) {
            res[idx] = finalRes[i];
        }
        ArrayUtils.reverse(res);
        return res;
    }

    private int[] leftShift(int[] num, int cntOfShift) {
        var res = new int[num.length + cntOfShift];
        for (int i = num.length; i < num.length + cntOfShift; ++i) {
            res[i] = 1;
        }

        System.arraycopy(num, 0, res, 0, num.length);
        return res;
    }

    public static void main(String[] args) {
        var binNumSub = new BinaryNumSubtract(BinaryNumSum.convertDecToBinary(99764923), BinaryNumSum.convertDecToBinary(175));
        var res = binNumSub.subtract();
        System.out.println(BinaryNumSum.convertBinaryToDec(res));
    }
}