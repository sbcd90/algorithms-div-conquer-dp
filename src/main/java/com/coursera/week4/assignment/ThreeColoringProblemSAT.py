from CNFSatisfiabilityProblem import SATInstance
from DPLLAlgorithm import solve_formula
from ThreeColoringProblem import UndirectedGraph, is_three_coloring
import time


# Input: a graph that is an instance of the `UndirectedGraph` class
# Output: an instance of `SATInstance` that encodes the 3 coloring problem
# Useful functions:
#   SATInstance class add_clause
#   UndirectedGraph class get_list_of_edges
def translate_three_coloring(graph):
    n_boolean_vars = graph.n * 3  # 3 boolean variables for each vertex
    # You can define your own scheme for translating x_i,j into the index of a prop. var.
    # we propose using x_i,j --> 3 *i + j
    s = SATInstance(n_boolean_vars, [])  # no clauses
    # your code here
    clauses = s.clauses
    for i in range(graph.n):
        clauses.append([3 * i + 1, 3 * i + 2, 3 * i + 3])
        clauses.append([-(3 * i + 1), -(3 * i + 2)])
        clauses.append([-(3 * i + 2), -(3 * i + 3)])
        clauses.append([-(3 * i + 1), -(3 * i + 3)])

    edges = graph.get_list_of_edges()
    for i in range(len(edges)):
        edge = edges[i]
        x = min(edge[0], edge[1])
        y = max(edge[0], edge[1])

        clauses.append([-(3 * x + 1), -(3 * y + 1)])
        clauses.append([-(3 * x + 2), -(3 * y + 2)])
        clauses.append([-(3 * x + 3), -(3 * y + 3)])
    s.clauses = clauses
    s.m = len(clauses)
    return s


# Input: graph --> an instance of UndirectedGraph with n vertices
#         truth_assign --> dictionary with key in range 1 ... 3*n mapping each key to true/false
#                           output from SAT solver.
# Output: A dictionary mapping vertices 0,..., n-1 to colors {1, 2, 3}
# This function will be implemented based on the scheme you used in previous function translate_three_coloring
def extract_graph_coloring_from_truth_assignment(graph, truth_assign):
    # your code here
    coloring = {}
    for i in range(graph.n):
        if (3 * i + 1) in truth_assign.keys() and truth_assign[3 * i + 1] is True:
            coloring[i] = 1
        elif (3 * i + 2) in truth_assign.keys() and truth_assign[3 * i + 2] is True:
            coloring[i] = 2
        elif (3 * i + 3) in truth_assign.keys() and truth_assign[3 * i + 3] is True:
            coloring[i] = 3
    return coloring


def solve_three_coloring(graph):
    s = translate_three_coloring(graph)
    print(s.clauses)
    res, truth_assign = solve_formula(s)
    print(res)
    print(truth_assign)
    if res:
        return extract_graph_coloring_from_truth_assignment(graph, truth_assign)
    else:
        return None


if __name__ == "__main__":
    print('--- Test 0 ---')
    # A simple triangle should be 3 colorable
    g0 = UndirectedGraph(3)
    g0.add_edge(0, 1)
    g0.add_edge(1, 2)
    g0.add_edge(0, 2)
    coloring = solve_three_coloring(g0)
    print(coloring)
    assert (coloring != None)
    assert (is_three_coloring(g0, coloring))
    print('Passed')

    print('-- Test 1 --')
    # The "complete" graph on 4 vertices is not 3 colorable
    g1 = UndirectedGraph(4)
    g1.add_edge(0, 1)
    g1.add_edge(0, 2)
    g1.add_edge(0, 3)
    g1.add_edge(1, 2)
    g1.add_edge(1, 3)
    g1.add_edge(2, 3)
    coloring = solve_three_coloring(g1)
    assert (coloring == None)
    print('Passed')

    print('--Test 2--')
    # Make a chordal graph on 6 vertices
    g2 = UndirectedGraph(6)
    # make a 6 cycle
    g2.add_edge(0, 1)
    g2.add_edge(1, 2)
    g2.add_edge(2, 3)
    g2.add_edge(3, 4)
    g2.add_edge(4, 5)
    # add two chords
    g2.add_edge(0, 3)
    g2.add_edge(2, 4)
    coloring = solve_three_coloring(g2)
    print(coloring)
    assert (coloring != None)
    assert (is_three_coloring(g2, coloring))
    print('Passed')

    print('-- Test 3 --')
    g2.add_edge(1, 3)
    g2.add_edge(0, 2)
    coloring = solve_three_coloring(g2)
    print(coloring)
    assert (coloring == None)
    print('Passed')

    print('--- Test 4 ---')
    g1 = UndirectedGraph(5)
    g1.add_edge(0, 1)
    g1.add_edge(1, 2)
    g1.add_edge(2, 0)
    g1.add_edge(1, 3)
    g1.add_edge(3, 4)
    g1.add_edge(1, 4)
    g1.add_edge(4, 0)
    coloring = solve_three_coloring(g1)
    print(coloring)
    assert (is_three_coloring(g1, coloring))
    print('Passed')

    print('-- Test 5 -- ')

    g2 = UndirectedGraph(7)
    g2.add_edge(2, 3)
    g2.add_edge(2, 1)
    g2.add_edge(2, 0)
    g2.add_edge(2, 4)
    g2.add_edge(3, 5)
    g2.add_edge(3, 6)
    g2.add_edge(5, 6)
    g2.add_edge(1, 0)
    g2.add_edge(1, 4)

    coloring = solve_three_coloring(g2)
    print(coloring)
    assert (is_three_coloring(g2, coloring))
    print('Passed')

    print('--Test 6--')
    start_time = time.time()
    g2.add_edge(0, 4)
    coloring = solve_three_coloring(g2)
    assert (coloring == None)
    print('passed')
    print(time.time() - start_time)

    print('All test passed: 15 points!')
