class UndirectedGraph:
    # n_verts: number of vertices of the graph
    #   vertices are labeled from 0... n-1
    # adj_list: an adjacency list represented as a list of lists.
    #  if set to None, we will initialize it as an empty graph
    def __init__(self, n_verts, adj_list=None):
        self.n = n_verts
        if adj_list == None:
            adj_list = [[] for j in range(self.n)]  # initialize as empty list of lists
        else:
            assert len(self.adj_list) == n_verts
            for lst in adj_list:
                for elt in lst:
                    assert 0 <= elt and elt < self.n_verts

        self.adj_list = adj_list

    def add_edge(self, i, j):
        assert 0 <= i and i < self.n
        assert 0 <= j and j < self.n
        assert i != j
        self.adj_list[i].append(j)
        self.adj_list[j].append(i)

    def get_list_of_edges(self):
        return [(i, j) for i in range(self.n) for j in self.adj_list[i] if i < j]


def is_three_coloring(graph, coloring):
    n = graph.n
    for i in range(n):
        if i not in coloring:
            return False  # every vertex must receive a color
        if coloring[i] < 1 or coloring[i] > 3:
            return False  # coloring must be between 1 and 3 inclusive
    # Your code should complete the check below
    # use the provided function graph.get_list_of_edges() to get a list of edges
    # or feel free to extend the graph data structure as you will.
    # your code here
    adj_list = graph.get_list_of_edges()
    for i in range(len(adj_list)):
        edge = adj_list[i]

        if coloring[edge[0]] == coloring[edge[1]]:
            return False
    return True


if __name__ == "__main__":
    print('--- Test 1 ---')
    g1 = UndirectedGraph(5)
    g1.add_edge(0, 1)
    g1.add_edge(1, 2)
    g1.add_edge(2, 0)
    g1.add_edge(1, 3)
    g1.add_edge(3, 4)
    g1.add_edge(1, 4)
    g1.add_edge(4, 0)

    coloring1 = {0: 1, 1: 2, 2: 3, 3: 1, 4: 3}
    assert (is_three_coloring(g1, coloring1), 'Test 1 fail: Coloring should be valid of g1.')

    print('--- Test 2 ---')
    g2 = UndirectedGraph(7)
    g2.add_edge(2, 3)
    g2.add_edge(2, 1)
    g2.add_edge(2, 0)
    g2.add_edge(2, 4)
    g2.add_edge(3, 5)
    g2.add_edge(3, 6)
    g2.add_edge(5, 6)
    g2.add_edge(1, 0)
    g2.add_edge(1, 4)
    g2.add_edge(0, 4)

    coloring2 = {2: 1, 3: 2, 4: 2, 0: 1, 1: 3, 5: 3, 6: 1}

    assert (not is_three_coloring(g2, coloring2), 'Test 2 fail: Coloring should be not be valid of g2.')

    print('-- Test 3 --- ')

    coloring3 = {2: 3, 3: 2, 4: 2, 0: 2, 1: 1, 5: 3, 6: 1}

    assert (not is_three_coloring(g2, coloring3), 'Test 3 fail: Coloring should be not be valid of g2.')

    print('All Tests Passed (10 points)!')
