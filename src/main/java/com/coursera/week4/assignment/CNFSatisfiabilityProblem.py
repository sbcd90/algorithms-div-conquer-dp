class SATInstance:
    # Constructor: provide n the number of variables and
    # an initial list of clauses.
    # Note that variable numbers will go from 1 to n inclusive.
    # we can add clauses using the add_clause method.
    def __init__(self, n, clauses):
        self.n = n
        self.m = len(clauses)
        self.clauses = clauses
        assert self.is_valid()

    # is_valid
    # Check if all clauses are correct.
    # literals in each clause must be between 1 and n or -n and -1
    def is_valid(self):
        assert self.n >= 1
        assert self.m >= 0
        for c in self.clauses:
            for l in c:
                assert (1 <= l and l <= self.n) or (-self.n <= l and l <= -1)
        return True

    # add_clause
    # Add a new clause to the list of clauses
    def add_clause(self, c):
        # check the clause we are adding.
        for l in c:
            assert (1 <= l and l <= self.n) or (-self.n <= 1 and l <= -1)
        self.clauses.append(c)

    ## Function: evaluate_literal
    # Evaluate a literal against a partial truth assignment
    # return 0 if the partial truth assignment does not have the variable corresponding to the literal
    # return 1 if the partial truth assignment has the variable and the literal is true
    # return -1 if the partial truth assignment has the variable and the literal is false
    def evaluate_literal(self, partial_truth_assignment, literal):
        var = abs(literal)  # literal may be negated. First remove any negation using abs
        if var not in partial_truth_assignment:
            return 0
        v = partial_truth_assignment[var]
        if 1 <= literal <= self.n:
            return 1 if v else -1
        else:
            return -1 if v else 1

    ## TODO: Write your code here
    # Function: evaluate
    # See description above: partial_truth_assignment is a dictionary from 1 .. n to true/false.
    # since it is partial, we may have variables with no truth assignments.
    # use evaluate_literal function as a useful primitive
    # return +1 if the formula is already satisfied under partial_truth_assignment: i.e, all clauses are true
    # return 0 if formula is indeterminate under partial_truth_assignment, all clauses are true or unresolved and at least one clause is unresolved.
    # return -1 if formula is already violated under partial_truth_assignment, i.e, at least one clause is false
    def evaluate(self, partial_truth_assignment):
        # your code here
        for i in range(self.m):
            clauseLen = len(self.clauses[i])
            valid = -1
            for j in range(clauseLen):
                eval_literal = self.evaluate_literal(partial_truth_assignment, self.clauses[i][j])
                if eval_literal == 1:
                    valid = 1
                    break
                elif eval_literal == 0:
                    valid = 0

            if valid != 1:
                return False
        return True


if __name__ == "__main__":
    ## BEGIN TESTS

    print('-test1-')
    f1 = SATInstance(4, [[1, 2, -4], [-2, -3, 1], [-1, -2, -3]])
    t1 = {1: True, 2: False}
    e1 = f1.evaluate(t1)
    assert (e1 == 1, f'Expected that f1 is satisfied by t1 but your code returns: {e1}')

    print('-test2-')
    t2 = {1: False, 2: False}
    e2 = f1.evaluate(t2)
    assert (e2 == 0, f'Expected that f1 is indeterminate under t2. Your code returns: {e2}')

    print('-test6-')
    f4 = SATInstance(9, [[1, 2, 3], [4, 5, 6], [7, 8, 9], [-1, -2], [-2, -3], [-1, -3], [-4, -5], [-5, -6], [-4, -6],
                         [-7, -8], [-8, -9], [-7, -9],
                         [-1, -4], [-2, -5], [-3, -6], [-1, -7], [-2, -8], [-3, -9], [-4, -7], [-5, -8], [-6, -9]])
    t6 = {1: False, 2: False, 3: False, 4: False, 5: True, 6: False, 7: False, 8: False, 9: True}
    e6 = f4.evaluate(t6)
    assert (e6 == 1, f'Expected that f4 is violated by t5. Your code returns {e6}')
    print('All tests passed: 10 points!')
    ## END TESTS
